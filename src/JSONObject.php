<?php

namespace Arinity\JSONObject;

class JSONObject {
    
    /** @var stdClass|array $_jsonData The parsed source JSON data */
    protected $_jsonData;

    /** @var object $_outputClass The class to serialize into */
    protected object $_outputClass;
    
    /** @var const RESERVED_WORDS A list of words to reserve when working with types */
    protected const RESERVED_WORDS = [
        'int',
        'bool',
        'string',
        'object',
        'array',
        'float',
    ];
    
    /** @var integer $_currentDepth The current depth of the node we are processing */
    protected int $_currentDepth = 0;
    
    /** @var array $_activeReflectionClassStack */
    protected array $_activeReflectionClassStack = [];
    
    /** @var array $_activePropertyStack */
    protected array $_activePropertyStack = [];
    
    /** @var array $_activeInstanceStack */
    protected array $_activeInstanceStack = [];
    
    /** @var array $_options */
    protected array $_options = [
        'verbose' => false,
    ];
    
    /**
     * Constructs a new JSONObject
     *
     * @param string $rawJSON A raw JSON string to parse and serialize
     * @param string $className The name of the class to serialize into
     * @param ?array $options An array of optional options
     *
     * @throws InvalidArgumentException
     *
     * @return Arinity\JSONObject\JSONObject
     */
    public function __construct(string $rawJSON, string $className, array $options = []) {
        $this->_jsonData = json_decode($rawJSON, true);
        if ($this->_jsonData === null) {
            throw new \InvalidArgumentException("Provided JSON cannot be parsed as JSON");
        }
        try {
            $this->_activeReflectionClassStack[] = $reflectorClass = new \ReflectionClass($className);
        } catch (\ReflectionException $e) {
            throw new \InvalidArgumentException("Provided class does not exist");
        }
        $this->_options = array_merge($this->_options, $options);
        $this->_outputClass = $this->_activeInstanceStack[] = $reflectorClass->newInstanceWithoutConstructor();
        $this->_parse();
    }
    
    /**
     * Checks if an array is sequential.
     *
     * This is only provided for compatibility with PHP 7.4 and 8.0. As of PHP 8.1, array_is_list was added
     * which does this check for us, and in the standard library.
     *
     * @param array $array The array to check
     * @return bool If the array is sequential. It can be assumed that it is associative if false
     */
    protected static function arrayIsSequential(array $array): bool {
        // Use array_is_list if we're being executed from PHP 8.1 or greater
        if (PHP_VERSION_ID >= 80100) {
            return array_is_list($array);
        }
        // If the array is empty or if the array's values are equal to the array, the array must be sequential
        if ($array === [] || $array === array_values($array)) {
            return true;
        }
        // Iterate over array keys to check for sequential keys
        $nextKey = -1;
        foreach ($array as $key => $value) {
            // if key is not equal to the last key + 1, it cannot be sequential
            if ($key !== ++$nextKey) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Extracts parameters from a DocBlock string.
     *
     * @param string $docBlock The DocBlock to extract parameters for
     * @param ?string $needle The specific parameter to return
     * @return array|string|null
     */
    protected function extractDocBlockParameters(string $docBlock, ?string $needle = null) {
        // Regex to match @ attributes in a DocBlock
        $propertyRegex = '/(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)/';
        preg_match_all($propertyRegex, $docBlock, $matches);
        $parameters = [];
        foreach ($matches[0] as $match) {
            // We only want parameters, which start with @. Continue on if this match does not
            if (strncmp($match, '@', 1) !== 0) {
                continue;
            }
            // Split name and value
            list($name, $value) = explode(' ', $match);
            // Remove @ from name
            $name = substr($name, 1);
            // Store the parameter
            $parameters[$name] = $value;
        }
        // Return the whole array if no needle was specified.
        if ($needle === null) {
            return $parameters;
        } else {
            // If the parameter specified as the needle exists, return it.
            if (array_key_exists($needle, $parameters) === true) {
                return $parameters[$needle];
            }
        }
        // Otherwise, return null.
        return null;
    }
     
    /**
     * Finds a matching ReflectionProperty for a JSON source property.
     *
     * findDestinationProperty() will first try to find an exact match. If it fails, it will then
     * attempt to find a parameter with a JSONObjectProperty attribute that has a matching
     * property name. If it cannot find a property from either of the above methods, it will return
     * null.
     *
     * @param string $sourceName The JSON property name
     * @param ReflectionClass $destinationClass The class we are searching for a matching property
     * @return ?ReflectionProperty
     */
    protected function findDestinationProperty(
        string $sourceName,
        \ReflectionClass $destinationClass
    ): ?\ReflectionProperty {
        $destinationProperty = null;
        try {
            // First attempt to find an exact match
            $destinationProperty = $destinationClass->getProperty($sourceName);
        } catch (\ReflectionException $e) {
            // Iterate over all of the classes properties to try to find a match
            foreach ($destinationClass->getProperties() as $objectProperty) {
                // If PHP > 8, attributes should be in use.
                if (PHP_VERSION_ID >= 80000) {
                    // Try to find a JSONObjectProperty attribute
                    $objectDefinitions = $objectProperty->getAttributes(Attributes\JSONObjectProperty::class);
                    $attribute = array_shift($objectDefinitions);
                    // If no attribute is found, continue to the next property. It's a lost cause
                    if ($attribute === null) {
                        continue;
                    }
                    // Gather any specified arguments for the attribute
                    $arguments = $attribute->getArguments();
                    // Check if the field argument exists. If it does, check if it matches the source name
                    if (
                        array_key_exists('field', $arguments) === true &&
                        $arguments['field'] === $sourceName
                    ) {
                        $destinationProperty = $objectProperty;
                        break;
                    }
                } else {
                    // Otherwise, check DocBlock.
                    $docBlock = $objectProperty->getDocComment();
                    // If no DocBlock is found, continue to the next property. It's a lost cause
                    if ($docBlock === false) {
                        continue;
                    }
                    // Check if @JSONProperty is a) defined, and b) set to the source name
                    $value = $this->extractDocBlockParameters($docBlock, 'JSONProperty');
                    if ($value === $sourceName) {
                        $destinationProperty = $objectProperty;
                        break;
                    }
                }
            }
        }
        return $destinationProperty;
    }
    
    /**
     * Checks an array typed parameter for an ArrayOf annotation or attribute.
     *
     * @param ReflectionProperty $property The property to check
     * @return ?string The specified array type, or null if not specified.
     */
    protected function findArrayOfForProperty(\ReflectionProperty $property): ?string {
        // If PHP >= 8, use attributes. Otherwise, use DocBlock
        if (PHP_VERSION_ID >= 80000) {
            // Try to find an ArrayOf definition
            $objectDefinitions = $property->getAttributes(Attributes\ArrayOf::class);
            $attribute = array_shift($objectDefinitions);
            // If no attribute is found, return null
            if ($attribute === null) {
                return null;
            }
            // Get the value of the attribute
            $arguments = $attribute->getArguments();
            $value = array_shift($arguments);
            // If it's not empty, return the value
            if (empty($value) === false) {
                return $value;
            }
            return null;
        } else {
            // Get DocBlock
            $docBlock = $property->getDocComment();
            // If no DocBlock is found, return null
            if ($docBlock === null) {
                return null;
            }
            // Check for @ArrayOf
            $value = $this->extractDocBlockParameters($docBlock, 'ArrayOf');
            // If it's not empty, return the value
            if (empty($value) === false) {
                return $value;
            }
            return null;
        }
    }
    
    /**
     * Checks a ReflectionClass for a property that has a true root attribute.
     *
     * @param ReflectionClass $class The ReflectionClass to check
     * @return ?ReflectionProperty The ReflectionProperty that has root attribute enabled, otherwise null
     */
    protected function findRootProperty(\ReflectionClass $class) {
        // Iterate over each property in the class
        foreach ($class->getProperties() as $property) {
            // Do we have a match?
            $potentialMatch = null;
            // If we're on PHP > 8, use attributes. Otherwise, use DocBlock
            if (PHP_VERSION_ID > 80000) {
                // Try to find a JSONObjectProperty attribute
                $objectDefinitions = $property->getAttributes(Attributes\JSONObjectProperty::class);
                $attribute = array_shift($objectDefinitions);
                // If no attribute is found, continue to the next property. It's a lost cause
                if ($attribute === null) {
                    continue;
                }
                // Gather any specified arguments for the attribute
                $arguments = $attribute->getArguments();
                // Check if the root argument exists
                if (array_key_exists('root', $arguments) === true) {
                    $potentialMatch = $arguments['root'];
                }
            } else {
                // Get DocBlock
                $docBlock = $property->getDocComment();
                // If no DocBlock is found, continue to the next property. It's a lost cause
                if ($docBlock === null) {
                    continue;
                }
                // Check for JSONRoot
                $potentialMatch = $this->extractDocBlockParameters($docBlock, 'JSONRoot');
            }
            // Is the value not null and true? If so, return our property. Otherwise, next.
            if ($potentialMatch === 'true' || $potentialMatch === true) {
                return $property;
            }
        }
        // No results
        return null;
    }
    
    /**
     * Fills an object instance variable with a specified value.
     *
     * @param object $class The instance that contains the property to fill
     * @param ReflectionProperty $property The ReflectionProperty instance to the property to fill
     * @param mixed $value The value to fill
     */
    protected function fillProperty(object $class, \ReflectionProperty $property, $value) {
        // Get the type of the output property
        $outputPropertyType = $property->getType();
        // This is only needed in 7, but doesn't hurt in 8. Set the variable as accessible.
        $property->setAccessible(true);
        /*
         * If the variable is an array, we may need to initialize it first. Additionally, we should append to arrays,
         * not overwrite.
         */
        if ($outputPropertyType instanceof \ReflectionNamedType && $outputPropertyType->getName() === 'array') {
            $outputArray = [];
            if ($property->isInitialized($class) === true) {
                $outputArray = $property->getValue($class);
            }
            $outputArray[] = $value;
            $property->setValue($class, $outputArray);
        } else {
            // @todo check if $property->getType() is the same as the value type, if not, try to convert it
            $property->setValue($class, $value);
        }
        // Mark the variable as inaccessible again.
        $property->setAccessible(false);
    }
    
    /**
     * Print a log line if debug is on.
     *
     * @param string $format The format of the message
     * @param ...mixed $args The arguments for this message
     */
    protected function printLogLine(string $message, ...$args) {
        // Bail if verbose is off
        if ($this->_options['verbose'] === false) {
            return;
        }
        // To make logs more readable, prepend tabs correlated to the current depth.
        $tabOutput = str_repeat("\t", ($this->_currentDepth - 1));
        // Do the logging
        $formattedMessage = sprintf($message, ...$args);
        printf("%s%s\n", $tabOutput, $formattedMessage);
    }
    
    /**
     * Parses a JSON node.
     *
     * parseJSONNode() is called recursively until it has completely iterated across the entire JSON document.
     *
     * @param array $node
     * @throw InvalidArgumentException
     * @return void
     */
    protected function parseJSONNode(array $node) {
        // If the node currently being processed is sequential or associative.
        $nodeIsSequential = self::arrayIsSequential($node);
        // Increase processing depth
        $this->_currentDepth++;
        // If we're at the root property and the node is sequential, find the root property the user desires.
        // The root property of the JSON document
        $rootProperty = null;
        if ($this->_currentDepth === 1 && $nodeIsSequential === true) {
            // Get the root reflection class
            $rootReflectionClass = end($this->_activeReflectionClassStack);
            // Try to find the root property definition
            $rootProperty = $this->_activePropertyStack[] = $this->findRootProperty($rootReflectionClass);
            // No luck, bail
            if ($rootProperty === null) {
                throw new \InvalidArgumentException(
                    sprintf(
                        "Provided JSON has no root element but lacks a root property definition in the '%s' class",
                        $rootReflectionClass->getName()
                    )
                );
            }
        }
        // If the current node is a sequential array, determine if we need to change types within the array to a class.
        $desiredClassType = null;
        if ($nodeIsSequential === true) {
            $desiredType = $this->findArrayOfForProperty(end($this->_activePropertyStack));
            if ($desiredType !== null) {
                // If it's a class, we need to check we can load the class
                try {
                    $desiredClassType = new \ReflectionClass($desiredType);
                } catch (\ReflectionException $e) {
                    throw new \InvalidArgumentException(sprintf("Cannot find type '%s'", $desiredType));
                }
            } else {
                // @todo handle no desired type (ArrayOf), or invalid
            }
        }
        // Iterate over each element in the node
        foreach ($node as $rootKey => $rootValue) {
            $this->printLogLine("Parsing %s", $rootKey);
            /*
             * Check to see if the current element is associative, if it is, we're assigning 1-1 to the
             * active property. If it's not, we're assigning to an array.
             */
            if ($nodeIsSequential === true && is_array($rootValue) === true) {
                $this->printLogLine("Root element is sequential, passing to parseJSONNode()");
                // If desired class type is set, append to active reflection class as we need to transform the array.
                if ($desiredClassType !== null) {
                    $this->_activeReflectionClassStack[] = $desiredClassType;
                    $className = $desiredClassType->getName();
                    $this->printLogLine("Creating new %s class", $className);
                    $this->_activeInstanceStack[] = $desiredClassType->newInstanceWithoutConstructor();
                }
                // Parse the node
                $this->parseJSONNode($rootValue);
                // Clean up afterwards
                if ($desiredClassType !== null) {
                    array_pop($this->_activeReflectionClassStack);
                }
                // Fill property on the root instance
                $instance = array_pop($this->_activeInstanceStack);
                $this->fillProperty(end($this->_activeInstanceStack), end($this->_activePropertyStack), $instance);
            } else {
                // Try to find the destination property.
                $property = end($this->_activePropertyStack);
                $class = end($this->_activeReflectionClassStack);
                if ($nodeIsSequential !== true) {
                    // If the node isn't sequential, we'll need to try to find the property it goes into in the class
                    $property = $this->findDestinationProperty(
                        $rootKey,
                        end($this->_activeReflectionClassStack)
                    );
                    if ($property === null) {
                        // @todo make this a configurable error
                        $this->printLogLine(
                            "Cannot find a matching property for '%s' in '%s'",
                            $rootKey,
                            $class->getName()
                        );
                        continue;
                    }
                    // Append to stack array.
                    $this->_activePropertyStack[] = $property;
                }
                // If value is an array, we need to go through parseJSONNode() again, otherwise it's ready to fill in
                if (is_array($rootValue) === true) {
                    // Check if destination property type is an object, if so, set our reflection class and instance
                    $isCustomObject = false;
                    if (
                        $property->getType() instanceof \ReflectionNamedType &&
                        $property->getType()->isBuiltIn() === false
                    ) {
                        // Set variables for this
                        $className = $property->getType()->getName();
                        $reflectionClass = $this->_activeReflectionClassStack[] = new \ReflectionClass($className);
                        $this->_activeInstanceStack[] = $reflectionClass->newInstanceWithoutConstructor();
                        $isCustomObject = true;
                    }
                    $this->printLogLine("%s is an array, passing on.", $rootKey);
                    $this->parseJSONNode($rootValue);
                    // If we're using a custom property, we need to set the property on the parent instance once ready
                    if ($isCustomObject === true) {
                        $instance = array_pop($this->_activeInstanceStack);
                        array_pop($this->_activeReflectionClassStack);
                        $this->fillProperty(
                            end($this->_activeInstanceStack),
                            end($this->_activePropertyStack),
                            $instance
                        );
                    }
                } else {
                    $this->printLogLine("Setting %s => %s in %s", $property->getName(), $rootValue, $class->getName());
                    $this->fillProperty(end($this->_activeInstanceStack), $property, $rootValue);
                }
            }
            // Remove the active property if node isn't sequential
            if ($nodeIsSequential === false) {
                $this->printLogLine("Removing last element from property stack.");
                array_pop($this->_activePropertyStack);
            }
        }
        // Decrease processing depth once we're done with this node
        $this->_currentDepth--;
    }
    
    /**
     * Internal parsing function.
     */
    protected function _parse() {
        $this->parseJSONNode($this->_jsonData);
    }
    
    /**
     * Helper function for external use.
     *
     * @param mixed ...$args Parameters to pass to the constructor
     * @throws InvalidArgumentException
     * @return object Requested output class
     */
    public static function parse(...$args) {
        $self = new self(...$args);
        return $self->_outputClass;
    }
}
